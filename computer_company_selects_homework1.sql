use computer_company;

select maker,type from product where type='Laptop' order by maker;
select model, ram, screen,price from laptop where price >1000 order by ram asc, price desc;
select * from printer where color='y' order by price;
select model, speed, hd, cd, price from pc where cd rlike '12x|24x' and price <600 order by speed desc;
select * from pc where speed >= 500 and price < 800 order by price desc;
select * from printer where price < 300 and type != 'Matrix';
select model, speed from pc where price between 400 and 600 order by hd asc;
select pc.model, pc.speed, pc.hd from pc,product where pc.hd between 10 and 20 and product.maker='A' and product.model = pc.model;
select model ,speed, hd, price, screen from laptop where screen > 12 order by price desc;
select model , type, price from printer where price < 300 order by type desc;
select model , ram ,price from laptop where ram = 64 order by screen;
select model , ram ,price from laptop where ram > 64 order by hd;
select model , speed, price, hd from pc where speed between 500 and 750 order by hd desc;

select model from pc where model like '%1%1%';

select product.maker, product.type, pc.speed, pc.hd from pc, product where pc.hd<=8 and product.model=pc.model;
select product.maker from pc,product where pc.model=product.model and pc.speed >=600;
select product.maker from laptop,product where laptop.model=product.model and laptop.speed <=500;
select lt1.model model1, lt2.model model2, lt1.hd, lt1.ram from laptop lt1, laptop lt2 where lt1.hd = lt2.hd and lt1.ram = lt2.ram and lt1.model>=lt2.model and lt1.code > lt2.code; 
select pc.model, product.maker from pc, product where pc.price <600 and pc.model=product.model;
select product.model, product.maker from printer, product where printer.price > 300 and printer.model = product.model;
select product.maker, product.model, pc.price from pc, product where product.model = pc.model order by product.maker;
select product.maker, product.model, pc.* from product right join pc on product.model = pc.model order by product.maker;
select product.maker, product.type, laptop.model, laptop.speed from laptop, product where laptop.speed >600 and laptop.model = product.model;
select product.maker,pc.* from product, pc where pc.price >500 and pc.model = product.model;

