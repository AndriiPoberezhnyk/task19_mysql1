use airport;

select trip_no, town_from, town_to from trip where town_from ='London' or town_to='London' order by time_out;
select trip_no, town_from, town_to from trip where plane='TU-134' order by time_out desc;
select trip_no, town_from, town_to from trip where plane != 'IL-86' order by plane asc;
select trip_no, town_from, town_to from trip where town_from !='Rostov' and town_to!='Rostov' order by plane asc;

select * from trip where hour(time_out) between 12 and 17;
select * from trip where hour(time_in) between 17 and 23;
select date, place from pass_in_trip where place like '1%';
select date, place from pass_in_trip where place like '%c';
select substring_index (name, ' ', -1) as surname from passenger where name rlike '^[^ ]+ C[^ ]+$';
select substring_index (name, ' ', -1) as surname from passenger where name rlike '^[^ ]+ [^J][^ ]+$';

select company.name, trip.plane from company,trip where company.ID_comp = trip.ID_comp and plane='Boeing';
select pass_in_trip.date, passenger.name from pass_in_trip, passenger where pass_in_trip.ID_psg = passenger.ID_psg;