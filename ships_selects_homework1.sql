use ships;

select name, class from ships order by name;
select * from classes where country = 'Japan' order by type desc;
select name, launched from ships where launched between 1920 and 1942 order by launched desc;
select ship, battle, result from outcomes where battle = 'Guadalcanal' order by ship desc;
select ship, battle, result from outcomes where result = 'sunk' order by ship desc;
select class, displacement from classes where displacement >= 40000 order by type;

select * from ships where name rlike '^W.*n$';
select * from ships where name rlike '^[^e]*e[^e]*e[^e]*$';
select name, launched from ships where name not like '%a';
select battle,ship from outcomes where ship rlike '^[^ ]+ [^ ]+[^c]$';

select c1.country, c1.type, c1.class, c2.type, c2.class from classes c1, classes c2 where c1.type ='bb' and c2.type='bc' order by c1.country;
select distinct c1.country, c1.type, c2.type from classes c1, classes c2 where c1.type ='bb' and c2.type='bc' order by c1.country;
select ships.name, classes.displacement from ships, classes where ships.class = classes.class order by classes.displacement desc;
select outcomes.ship, outcomes.battle, battles.date from battles, outcomes where outcomes.result !='sunk' and outcomes.battle = battles.name;
select ships.name, classes.country from ships left join classes on ships.class = classes.class;