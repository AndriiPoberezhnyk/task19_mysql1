use computer_company;

select model ,speed, hd, price from pc where price<500 order by speed;
select pc.model, pc.speed, pc.hd, pc.price from pc, product  where pc.model = product.model and product.type ='PC' and pc.price < 500  order by speed;

select distinct maker, type from product where type ='Printer' order by maker DESC;
select distinct product.maker, product.type from product, printer where product.model = printer.model order by product.maker DESC;

select maker, type from product where type='PC' order by maker desc;

select product.maker, product.type, laptop.speed, laptop.hd from product, laptop where laptop.hd >= 10 and product.model = laptop.model;

select pc1.model, pc2.model, pc1.speed, pc1.ram from pc as pc1 , pc as pc2 where pc1.speed = pc2.speed and pc1.ram = pc2.ram and pc1.model > pc2.model;

select * from pc left join laptop on pc.speed = laptop.speed;
select * from pc right join laptop on pc.speed = laptop.speed;
select * from pc cross  join laptop on pc.speed = laptop.speed;

select * from pc left join laptop on pc.speed = laptop.speed where pc.hd >=10;
select * from pc right join laptop on pc.speed = laptop.speed where pc.hd >=10;
select * from pc cross  join laptop on pc.speed = laptop.speed where pc.hd >=10;